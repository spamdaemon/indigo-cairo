#include <indigo/cairo/ImageRenderContext.h>
#include <indigo/cairo/RenderContext.h>
#include <timber/media/ImageBuffer.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::media;
using namespace ::timber::logging;
using namespace ::indigo::render;

namespace indigo {
   namespace cairo {

      namespace {
         static Log logger()
         {
            return "indigo.cairo.ImageRenderContext";
         }

         struct ContextImpl : public OffscreenImageContext
         {

               ContextImpl(cairo_surface_t* s, size_t w, size_t h, const Color& bg) throws()
               : _surface(s)
               {
                  assert (cairo_image_surface_get_format(_surface) != CAIRO_FORMAT_INVALID);

                  cairo_t* ctx = cairo_create(s);

                  // clear the image with the background color
                  cairo_set_source_rgba(ctx,bg.red(),bg.green(),bg.blue(),bg.opacity());
                  cairo_paint(ctx);

                  _context=RenderContext::create(ctx,0,0,w,h,w,h);
                  // we can safely destroy the context
                  cairo_destroy(ctx);
               }

               ~ContextImpl()throws()
               {
                  cairo_surface_destroy(_surface);
               }

               Context& renderContext()throws()
               {
                  return *_context;
               }

               ::timber::SharedRef< Image> getImage()throws()
               {
                  ::std::shared_ptr<Image> res = ImageRenderContext::getSurfaceImage(_surface);
                  if (!res) {
                     logger().severe("Could not convert cairo surface to image; aborting");
                     ::std::abort();
                  }
                  return res;
               }

            private:
               cairo_surface_t *_surface;

               /** The render context */
            private:
               unique_ptr< Context> _context;
         };

      }

      unique_ptr< OffscreenImageContext> ImageRenderContext::createARGB32Context(unsigned int w, unsigned int h,
            const Color& background)
      throws (invalid_argument)
      {
         if (w==0 || h==0) {
            throw invalid_argument("Invalid size");
         }
         cairo_surface_t* s = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h);
return      unique_ptr<OffscreenImageContext>(new ContextImpl(s,w,h,background));
   }

   ::std::shared_ptr< Image> ImageRenderContext::getSurfaceImage(cairo_surface_t* s)throws()
   {
      if (cairo_image_surface_get_format(s) != CAIRO_FORMAT_ARGB32) {
         LogEntry(logger()).warn() << "Unsupported surface format " << cairo_image_surface_get_format(s) << "; only CAIRO_FORMAT_ARGB32 is supported" << doLog;
         return  ::std::shared_ptr< Image>();
      }

      const bool traceEnabled = logger().isLoggable(Level::TRACING);

      cairo_surface_flush(s);

      const size_t w = cairo_image_surface_get_width(s);
      const size_t h = cairo_image_surface_get_height(s);
      const size_t stride = cairo_image_surface_get_stride(s);

      const unsigned char* data = cairo_image_surface_get_data(s);

      ::std::unique_ptr<ImageBuffer::PixelColor[]> pixels ( new ImageBuffer::PixelColor[w*h]);
      ImageBuffer::PixelColor* out = pixels.get();
      for (size_t y=0;y<h;++y) {
         const uint32_t* row = reinterpret_cast< const uint32_t*>(data + stride * y);
         for (size_t x=0;x<w;++x) {
            const uint32_t pixel = (*row++);

            // since the pixels are pre-multiplied by alpha, we need to undo the effect and
            // make sure the value are in the proper range
            uint32_t a = (pixel>>24) & 0xff;
            uint32_t r = ::std::min(255u,(((pixel>>16) & 0xff)*255)/a);
            uint32_t g = ::std::min(255u,(((pixel>>8) & 0xff)*255)/a);
            uint32_t b = ::std::min(255u,(((pixel>>0) & 0xff)*255)/a);

            if(traceEnabled) {
               LogEntry(logger()).tracing() << "argb32 : " << ((pixel>>24) & 0xff) << ", " << ((pixel>>16) & 0xff) << ", " << ((pixel>>8) & 0xff) << ", " << ((pixel>>0) & 0xff) << doLog;
            }

            out->_blue = b*257;
            out->_green = g*257;
            out->_red = r*257;
            out->_opacity = a*257;

            ++out;
         }
      }

      return ImageBuffer::createImage(w,h,move(pixels));
   }

}

}
