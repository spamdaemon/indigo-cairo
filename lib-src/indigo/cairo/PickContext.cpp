#include <indigo/cairo/PickContext.h>
#include <indigo/cairo/RenderContext.h>

using namespace ::std;

namespace indigo {
   namespace cairo {
      namespace {

         struct PickContextImpl : public ::indigo::render::PickContext
         {

               /**
                * @param surface the cairo surface
                * @param r The pick renderer
                */
               PickContextImpl(size_t width, size_t height, size_t px, size_t py)throws()
               {
                  _surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32,width,height);
                  _ctx = cairo_create(_surface);

                  // setup the a clip area the size of the point
                  cairo_move_to(_ctx,px-0.5,py-0.5);
                  cairo_line_to(_ctx,px-0.5,py+0.5);
                  cairo_line_to(_ctx,px+0.5,py+0.5);
                  cairo_line_to(_ctx,px+0.5,py-0.5);
                  cairo_close_path(_ctx);
                  cairo_clip(_ctx);

                  // we render into a 3x3 grid
                  _context = RenderContext::create(_ctx,px-0.5,py-.5,1,1,width,height);

                  // at this point, we compute the pixel
                  const size_t offset = cairo_image_surface_get_stride(_surface) * py + 4*px;
                  _pixel = reinterpret_cast<unsigned int*>(cairo_image_surface_get_data(_surface) + offset);

                  // use test and reset to initialize the pixel
                  testAndReset();
               }

               ~PickContextImpl()throws() {
                  cairo_destroy(_ctx);
                  cairo_surface_destroy(_surface);
               }

               ::indigo::render::Context& renderContext()throws()
               {  return *_context;}

               bool testAndReset()throws()
               {
                  // need to flush output to the surface before we can test it
                  cairo_surface_flush(_surface);

                  // we can take advantage of the fact that the pixels are pre-multiplied by alpha
                  // which means if a alpha==0, then the pixel will be identically 0
                  if (*_pixel==0) {
                     return false;
                  }
                  else {
                     *_pixel = 0; // clear the pixel completely i.e. it's transparent
                     cairo_surface_mark_dirty(_surface);
                     return true;
                  }
               }

               /** The surface */
            private:
               cairo_surface_t* _surface;

               /** The context */
            private:
               cairo_t* _ctx;

               /** The ARGB data buffer that will be modified and read throughout the pick operation */
            private:
               unsigned int* _pixel;

            private:
               unique_ptr< ::indigo::render::Context> _context;
         };

      }
      unique_ptr< ::indigo::render::PickContext> PickContext::create(size_t width, size_t height, size_t px, size_t py)
throws   ()
   {
      return unique_ptr< ::indigo::render::PickContext>(new PickContextImpl(width,height,px,py));
   }

}
}
