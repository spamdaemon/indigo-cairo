#ifndef _INDIGO_CAIRO_IMAGERENDERCONTEXT_H
#define _INDIGO_CAIRO_IMAGERENDERCONTEXT_H

#ifndef _INDIGO_RENDER_OFFSCREENIMAGECONTEXT_H
#include <indigo/render/OffscreenImageContext.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#include <cairo.h>

namespace indigo {
   namespace cairo {

      /**
       * This class maybe used to create an render context for painting into
       * an offscreen image via cairo.
       */
      class ImageRenderContext
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(ImageRenderContext);

         private:
            ImageRenderContext();
            ~ImageRenderContext();

            /**
             * Create a new image render context for an ARGB32 buffer with the given width and height.
             * @param w the width of the canvas
             * @param h the height of the canvas
             * @param background the background color
             * @return a context or 0 if it wasn't created
             * @throws std::invalid_argument if either of width or height is 0
             */
         public:
            static ::std::unique_ptr< ::indigo::render::OffscreenImageContext> createARGB32Context(unsigned int w,
                  unsigned int h, const ::indigo::Color& background = ::indigo::Color::TRANSPARENT_BLACK)
            throws( ::std::invalid_argument);

            /**
             * This utility function converts a cairo_surface to an image.
             * @param surface a surface
             * @return an image or 0 if an error occurred
             */
         public:
            static ::std::shared_ptr< ::timber::media::Image> getSurfaceImage(cairo_surface_t* surface)throws();
      };
   }
}

#endif

