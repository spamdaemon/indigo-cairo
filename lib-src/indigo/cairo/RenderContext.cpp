#include <indigo/cairo/RenderContext.h>
#include <indigo/render/AbstractContext.h>
#include <indigo/Color.h>
#include <indigo/AntiAliasingMode.h>
#include <timber/logging.h>
#include <timber/media/ImageBuffer.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::media;
using namespace ::timber::logging;
using namespace ::indigo::render;

namespace indigo {
   namespace cairo {
      namespace {
         static Log logger()
         {
            return "indigo.cairo.RenderContext";
         }

         struct CairoContext : public AbstractContext
         {
               CairoContext(cairo_t* context, double x, double y, double w, double h, double sw, double sh)throws()
                     : AbstractContext(x, y, w, h, { 0, 0, sw, sh }), _context(cairo_reference(context)), _rgba(0)
               {
               }

               ~CairoContext()throws()
               {
                  cairo_destroy(_context);
               }

               void updateFillColor()
               {
                  if (_rgba != &_rgbaFill) {
                     _rgba = &_rgbaFill;
                     cairo_set_source_rgba(_context, _rgba->red(), _rgba->green(), _rgba->blue(),
                           _rgba->opacity() * alpha());
                  }
               }

               void updateStrokeColor()
               {
                  if (_rgba != &_rgbaStroke) {
                     _rgba = &_rgbaStroke;
                     cairo_set_source_rgba(_context, _rgba->red(), _rgba->green(), _rgba->blue(),
                           _rgba->opacity() * alpha());
                  }
               }

               void setAlpha(double a)throws()
               {
                  AbstractContext::setAlpha(a);
                  _rgba = nullptr;
               }

               void paintTransformedPoints(PaintMode mode, size_t n, const Point* pts, double ptSize)throws()
               {
               }

               void paintTransformedPolygon(PaintMode mode, size_t n, const Point* pts, bool convex)throws()
               {
                  cairo_new_path(_context);
                  for (const Point* p = pts, *end = pts + n; p != end; ++p) {
                     cairo_line_to(_context, p->x(), p->y());
                  }
                  cairo_close_path(_context);
                  if (mode & FILL) {
                     updateFillColor();
                     cairo_fill_preserve(_context);
                  }
                  if (mode & DRAW) {
                     updateStrokeColor();
                     cairo_stroke(_context);
                  }
               }

               void paintTransformedPolyline(PaintMode mode, size_t n, const Point* pts, bool close)throws()
               {
                  cairo_new_path(_context);
                  for (const Point* p = pts, *end = pts + n; p != end; ++p) {
                     cairo_line_to(_context, p->x(), p->y());
                  }
                  if (close) {
                     cairo_close_path(_context);
                  }
                  updateStrokeColor();
                  cairo_stroke(_context);
               }

               void setAntiAliasingMode(AntiAliasingMode mode)throws()
               {
                  switch (mode) {
                     case AntiAliasingMode::BEST:
                        cairo_set_antialias(_context, CAIRO_ANTIALIAS_SUBPIXEL);
                        break;
                     case AntiAliasingMode::FAST:
                        cairo_set_antialias(_context, CAIRO_ANTIALIAS_GRAY);
                        break;
                     case AntiAliasingMode::NONE:
                        cairo_set_antialias(_context, CAIRO_ANTIALIAS_NONE);
                        break;

                  }
               }

               void setFont(const ::timber::Reference< Font>& font)throws()
               {

               }

               void setSolidFillColor(const Color& fillColor)throws()
               {
                  if (_rgba == &_rgbaFill) {
                     _rgba = nullptr;
                  }
                  _rgbaFill = fillColor;
               }

               void setStrokeColor(const Color& strokeColor)throws()
               {
                  if (_rgba == &_rgbaStroke) {
                     _rgba = nullptr;
                  }
                  _rgbaStroke = strokeColor;
               }

               void setStrokeWidth(double width)throws()
               {
                  cairo_set_line_width(_context, width);
               }

               cairo_surface_t* createImage(const ImageRef& image)
               {
                  union
                  {
                        ::std::uint32_t argb;
                        unsigned char c[4];
                  } pixel;

                  cairo_surface_t* s = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, image->width(), image->height());
                  cairo_surface_flush(s);
                  unsigned char* buf = cairo_image_surface_get_data(s);

                  ImageBuffer::PixelColor col;
                  for (size_t y = 0; y < image->height(); ++y, buf += cairo_image_surface_get_stride(s)) {
                     unsigned char* row = buf;
                     for (size_t x = 0; x < image->width(); ++x) {
                        image->pixel(x, y, col);

                        // pre-scale all values by the opacity; the range is still that of an uint16
                        ::std::uint32_t a = col._opacity;
                        ::std::uint32_t r = (a * col._red) / ImageBuffer::MAX_OPACITY;
                        ::std::uint32_t g = (a * col._green) / ImageBuffer::MAX_OPACITY;
                        ::std::uint32_t b = (a * col._blue) / ImageBuffer::MAX_OPACITY;

                        pixel.argb = (a / 257) << 24;
                        pixel.argb |= (r / 257) << 16;
                        pixel.argb |= (g / 257) << 8;
                        pixel.argb |= (b / 257);

                        row[0] = pixel.c[0];
                        row[1] = pixel.c[1];
                        row[2] = pixel.c[2];
                        row[3] = pixel.c[3];

                        row += 4;
                     }
                  }
                  // since we're changing the surface, we have to mark it as dirty, otherwise
                  // optimizations in cairo don't recognize the new image contents
                  cairo_surface_mark_dirty(s);
                  return s;
               }

               void drawImage(double x, double y, const ImageRef& image, ::std::int32_t w, ::std::int32_t h)throws()
               {
                  if (image->width() == 0 || image->height() == 0) {
                     return;
                  }

                  _rgba = nullptr;
                  Point pt = AbstractContext::transformPoint(x, y);
                  //LogEntry(logger()).info() << "Transformed : " << x << ", " << y << " --> " << pt.x() << ", " << pt.y() << doLog;
                  cairo_matrix_t m;
                  cairo_matrix_init_translate(&m, pt.x(), pt.y());
                  double sx = w < 0 ? 1.0 : ((double) w) / image->width();
                  double sy = h < 0 ? 1.0 : ((double) h) / image->height();

                  if (sx != 1 || sy != 1) {
                     cairo_matrix_scale(&m, sx, sy);
                  }

                  // TODO: check if the image is actually visible
                  cairo_surface_t* s = createImage(image);
                  cairo_pattern_t* pattern = cairo_pattern_create_for_surface(s);
                  cairo_matrix_invert(&m);
                  cairo_pattern_set_matrix(pattern, &m);
                  cairo_set_source(_context, pattern);
                  cairo_paint_with_alpha(_context, alpha());
                  cairo_pattern_destroy(pattern);
                  cairo_surface_destroy(s);
               }

               void drawImage(double x, double y, const ImageRef& image, const AffineTransform2D& t)throws()
               {
                  if (t.isIdentityTransform()) {
                     drawImage(x, y, image, image->width(), image->height());
                     return;
                  }
                  _rgba = nullptr;
                  // TODO: check if the image is actually visible
                  Point pt = AbstractContext::transformPoint(x, y);
                  // LogEntry(logger()).info() << "Transformed : " << x << ", " << y << " --> " << pt.x() << ", " << pt.y() << doLog;
                  cairo_matrix_t m;
                  cairo_matrix_t n;
                  cairo_matrix_init_translate(&m, pt.x(), pt.y());
                  cairo_matrix_init(&n, t(0, 0), t(1, 0), t(0, 1), t(1, 1), t(0, 2), t(1, 2));
                  cairo_matrix_multiply(&m, &n, &m);
                  cairo_surface_t* s = createImage(image);
                  cairo_pattern_t* pattern = cairo_pattern_create_for_surface(s);
                  cairo_matrix_invert(&m);
                  cairo_pattern_set_matrix(pattern, &m);
                  cairo_set_source(_context, pattern);
                  cairo_paint_with_alpha(_context, alpha());
                  cairo_pattern_destroy(pattern);
                  cairo_surface_destroy(s);
               }

               void drawTextUtf8(double x, double y, const ::std::string& utf8)throws ()
               {
                  cairo_matrix_t m;
                  Point pt = AbstractContext::transformPoint(x, y);
                  cairo_matrix_init_translate(&m, pt.x(), pt.y());
                  cairo_set_font_matrix(_context, &m);
                  cairo_show_text(_context, utf8.c_str());
               }

               /** The current context */
            private:
               cairo_t* _context;

               /** The current color(null or rgbaStroke or rgbaFill) */
            private:
               Color* _rgba;

               /** The current stroke color */
            private:
               Color _rgbaStroke;

               /** The current fill color */
            private:
               Color _rgbaFill;
         };
      }

      unique_ptr< Context> RenderContext::create(cairo_t* context, double x, double y, double w, double h, size_t sw,
            size_t sh)
            throws ()
      {
         static bool haveCairoVersion = false;
         if (!haveCairoVersion) {
            haveCairoVersion = true;
            LogEntry(logger()).info() << "Creating Cairo renderer with version " << cairo_version_string() << doLog;
         }
         return unique_ptr < Context > (new CairoContext(context, x, y, w, h, sw, sh));
      }

   }
}
