#ifndef _INDIGO_CAIRO_PICKCONTEXT_H
#define _INDIGO_CAIRO_PICKCONTEXT_H

#ifndef _INDIGO_RENDER_PICKCONTEXT_H
#include <indigo/render/PickContext.h>
#endif

namespace indigo {
   namespace cairo {

      /**
       * This class is an implementation of the indigo rendering context for the CAIRO library.
       */
      class PickContext
      {
            /**
             * Constructor.
             * @param
             */
         private:
            PickContext()throws();

            /**
             * The destructor
             */
         private:
            ~PickContext()throws();

            /**
             * Create a new RenderContext for a Cairo surface.
             * @param width the width of the pick surface
             * @param height the height of the pick surface
             * @param px the point on the surface to pick (x-coordinate)
             * @param py the point on the surface to pick (y-coordinate)
             * @param context a cairo context
             * @return a render context
             */
         public:
            static ::std::unique_ptr< ::indigo::render::PickContext> create(size_t width, size_t height, size_t px, size_t py)throws();

      };

   }
}

#endif
