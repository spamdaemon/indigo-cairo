#ifndef _INDIGO_CAIRO_RENDERCONTEXT_H
#define _INDIGO_CAIRO_RENDERCONTEXT_H

#ifndef _INDIGO_RENDER_CONTEXT_H
#include <indigo/render/Context.h>
#endif

#include <memory>
#include <cairo.h>

namespace indigo {
   namespace cairo {

      /**
       * This class is an implementation of the indigo rendering context for the CAIRO library.
       */
      class RenderContext
      {
         private:
            RenderContext();
            ~RenderContext();

            /**
             * Create a new RenderContext for a Cairo surface. This call will increase the
             * context's reference count.
             * @param context a cairo context
             * @return a render context
             */
         public:
            static ::std::unique_ptr< ::indigo::render::Context> create(cairo_t* context, double x, double y,
                  double w, double h, size_t sw, size_t sh)throws();

      };

   }
}

#endif
