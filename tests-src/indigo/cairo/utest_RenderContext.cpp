#include <indigo/cairo/RenderContext.h>
#include <cassert>

using namespace ::std;
using namespace ::timber;
using namespace ::indigo::render;
using namespace ::indigo::cairo;

void utest_createContext()
{
  cairo_surface_t* surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32,100,100);
  cairo_t* context = cairo_create(surface);
  unique_ptr<Context> c = RenderContext::create(context,0,0,100,100,10,10);
  assert (c.get());
  c.reset(0);
  cairo_destroy(context);
  cairo_surface_destroy(surface);
}
