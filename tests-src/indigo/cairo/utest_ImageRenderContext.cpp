#include <indigo/cairo/ImageRenderContext.h>
#include <timber/media/ImageBuffer.h>
#include <timber/logging.h>
#include <cassert>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::media;
using namespace ::timber::logging;
using namespace ::indigo;
using namespace ::indigo::render;
using namespace ::indigo::cairo;

void utest_createContext()
{
   unique_ptr< OffscreenImageContext> ctx = ImageRenderContext::createARGB32Context(1, 2, Color::RED);
   SharedRef< ImageBuffer> buf = ctx->getImage();

   assert(buf->width() == 1);
   assert(buf->height() == 2);

   ImageBuffer::PixelColor p;
   buf->pixel(0, 0,p);
   assert(p._red ==  ImageBuffer::MAX_OPACITY);
   assert(p._green == 0);
   assert(p._blue == 0);
   assert(p._opacity ==  ImageBuffer::MAX_OPACITY);
}

void utest_backgroundWithAlpha()
{
   Log("indigo.cairo.ImageRenderContext").setLevel(Level::TRACING);

   Color col(1.0,0,0,.5);

   unique_ptr< OffscreenImageContext> ctx = ImageRenderContext::createARGB32Context(1, 1, col);
   SharedRef< ImageBuffer> buf = ctx->getImage();

   assert(buf->width() == 1);
   assert(buf->height() == 1);

   ImageBuffer::PixelColor p;
   buf->pixel(0, 0,p);
   assert(p._red ==  ImageBuffer::MAX_OPACITY);
   assert(p._green == 0);
   assert(p._blue == 0);
   LogEntry("indigo.cairo.ImageRenderContext").info() << "Opacity " << p._opacity << doLog;
   // NOTE: that we cannot express 50 of 255 in integers, and cairo is rounding up to 128,
   // but 128/255 is now  > 50% and we end up not with ImageBuffer::MAX_OPACITY/2 but something a little bit larger
   assert(p._opacity ==  128*257);
}

